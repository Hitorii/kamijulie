﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CameraExtensions
{
    public static Vector2 GetSizeAtDistance(this Camera camera, float distance)
    {
        var frustumHeight = 2.0f * distance * Mathf.Tan(camera.fieldOfView * 0.5f * Mathf.Deg2Rad);
        return new Vector2(frustumHeight * camera.aspect, frustumHeight);
    }
}
