﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class MoveToClickInput : MonoBehaviour
{

    public Vector2 speed = new Vector2(5f, 2f);

    // la position ou je clique 
    public Vector2 targetPosition;
    // Direction de la cible au joueur
    public Vector2 dirToTarget;

    // Store the movement / le mouvement 
    private Vector2 movement;

    private float targetX;

    private void Awake()
    {
        targetX = transform.position.x;
    }

    void Update()
    {
        //  Retrieve the mouse position
        // recupérer la position de la souris 
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            var sizeAtZero = Camera.main.GetSizeAtDistance(Mathf.Abs(Camera.main.transform.position.z));
            var viewportXMouse = Camera.main.ScreenToViewportPoint(Input.mousePosition).x;
            targetX = viewportXMouse * sizeAtZero.x + Camera.main.transform.position.x - sizeAtZero.x/2;
        }
        
        dirToTarget = new Vector2(targetX, transform.position.y) - (Vector2)transform.position;
    }

    void FixedUpdate()
    {
        //  If you are about to overshoot the target, reduce velocity to that distance
        // Si on est sur le point de dépasser la cible, on réduit la vélocité à cette distance 
        //      Else cap the Movement by a maximum speed per direction (x then y)
        //Sinon, on limite le mouvement à une vitesse maximale par direction (x puis y)
        if (speed.x * Time.deltaTime >= Mathf.Abs(dirToTarget.x))
        {
            movement.x = dirToTarget.x;
        }
        else
        {
            movement.x = speed.x * Mathf.Sign(dirToTarget.x);
        }

        //if (speed.y * Time.deltaTime >= Mathf.Abs(dirToTarget.y))
        //{
        //    movement.y = dirToTarget.y;
        //}
       
        GetComponent<Rigidbody2D>().velocity = movement;


        
    }
}

