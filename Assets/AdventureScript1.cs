﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdventureScript1 : MonoBehaviour
{ 
    public Vector3 mousePos;
    public Camera mainCamera;
    public Vector3 mousePosWorld;
    public Vector2 mousePosWorld2D;
    RaycastHit2D hit;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //check if mouse button was pressed
        if (Input.GetMouseButtonDown(0))
        {
            print("Button mouse was pressed");

            mousePos = Input.mousePosition;

            print("Screen Space:" + mousePos);

            mousePosWorld = mainCamera.ScreenToWorldPoint(mousePos);

            print("World Space:" + mousePosWorld);

            mousePosWorld2D = new Vector2(mousePosWorld.x, mousePosWorld.y);


            hit = Physics2D.Raycast(mousePosWorld2D, Vector2.zero) ;

            if (hit.collider != null)
            {
                print("Object is gonna collide");
                
                print("Name: " + hit.collider.gameObject.name);
            }
           // else
           // {
           //     print("Kein Collider erkannt!");
           // }


        }
    }
}
